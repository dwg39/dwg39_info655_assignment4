import React from 'react';

const PlayPauseButton = ({ onClick, isPlaying }) => {
  return (
    <button onClick={onClick}>{isPlaying ? 'Pause' : 'Play'}</button>
  );
};

export default PlayPauseButton;
