// Podcast.js
import React from 'react';

const Podcast = ({ season, episode, episodeTitle, onDoubleClick }) => {
  return (
    <div className="podcast" onDoubleClick={() => onDoubleClick(episodeTitle, true)}>
      <h3>{episodeTitle || 'Unknown Title'}</h3>
      <p>Season: {season ? season : 'N/A'}</p>
      <p>Episode: {episode ? episode : 'N/A'}</p>
    </div>
  );
};

export default Podcast;
