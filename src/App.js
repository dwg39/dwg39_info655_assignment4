import React from 'react';
import Playlist from './Playlist';

function App() {
  return (
    <div>
      <h1>David Gordon | INFO655 | Homework 3 | Drexel University | 02.22.2024</h1>
      <Playlist /> 
    </div>
  );
}

export default App;
