import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Song from './song';
import Podcast from './Podcast';
import Status from './Status'; 

import './Playlist.css'; 

const Playlist = () => {
  const [data, setData] = useState([]);
  const [status, setStatus] = useState('');
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    axios.get('/audio.json')
      .then((response) => {
        console.log('Fetched data:', response.data);
        if (Array.isArray(response.data.tracks)) {
          setData(response.data.tracks);
        } else {
          console.error('Fetched data does not contain an array:', response.data);
        }
      })
      .catch((error) => {
        console.error('Error fetching JSON:', error);
      });
  }, []);
  

  const handleDoubleClick = (audioTitle, isPodcast, episodeTitle) => {
    const audioType = isPodcast ? 'Podcast' : 'Song';
    setStatus(`Playing ${audioType}: ${audioTitle || episodeTitle || 'Unknown Title'}`);
  };

  const handleNextClick = () => {
    setCurrentIndex((prevIndex) => (prevIndex + 1) % data.length);
    const nextItem = data[(currentIndex + 1) % data.length];
    const nextTitle = ('episode' in nextItem) ? nextItem.episodeTitle : nextItem.title;
    const audioType = ('episode' in nextItem) ? 'Podcast' : 'Song';
    setStatus(`Playing ${audioType}: ${nextTitle}`);
  };

  const handlePrevClick = () => {
    setCurrentIndex((prevIndex) => (prevIndex - 1 + data.length) % data.length);
    const prevItem = data[(currentIndex - 1 + data.length) % data.length];
    const prevTitle = ('episode' in prevItem) ? prevItem.episodeTitle : prevItem.title;
    const audioType = ('episode' in prevItem) ? 'Podcast' : 'Song';
    setStatus(`Playing ${audioType}: ${prevTitle}`);
  };

  const handlePlayPauseClick = () => {
    if (status.includes('Playing')) {
      setStatus('Paused');
    } else if (status.includes('Paused')) {
      setStatus(`Playing: ${data[currentIndex].title}`);
    }
  };

  const handleShuffleClick = () => {
    const shuffledData = [...data].sort(() => Math.random() - 0.5);
    setData(shuffledData);
    setCurrentIndex(0);
    setStatus(`Shuffled Playlist`);
  };

  return (
    <div>
      <div className="buttons-container">
        <button onClick={handlePrevClick} className="control-button">Prev</button>
        <button onClick={handlePlayPauseClick} className="control-button">Play/Pause</button>
        <button onClick={handleNextClick} className="control-button">Next</button>
        <button onClick={handleShuffleClick} className="control-button">Shuffle</button>
      </div>

      <div className="status-container">
        <h2>Status:</h2>
        <Status statusText={status} />
      </div>

      <div className="playlist-container">
        {data.map((item, index) => (
          <div key={index} className="audio-container">
            <button onDoubleClick={() => handleDoubleClick(item.title, 'episode' in item || 'season' in item, item.episodeTitle)} className="audio-button">
              {('episode' in item || 'season' in item) ? <Podcast {...item} onDoubleClick={handleDoubleClick} /> : <Song {...item} onDoubleClick={handleDoubleClick} />}
            </button>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Playlist;
