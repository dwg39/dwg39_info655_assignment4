import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Song from './Song';

describe('Song Component', () => {
  test('renders correctly with valid props', () => {
    render(<Song title="Imagine" artist="John Lennon" year={1971} />);
    expect(screen.getByText('Imagine')).toBeInTheDocument();
    expect(screen.getByText('John Lennon')).toBeInTheDocument();
    expect(screen.getByText('1971')).toBeInTheDocument();
  });

  test('renders correctly with invalid year prop', () => {
    render(<Song title="Imagine" artist="John Lennon" year="1971" />);
    expect(screen.getByText('Unknown Year')).toBeInTheDocument();
  });
});
