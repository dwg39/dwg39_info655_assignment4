import React from 'react';

const Status = ({ statusText }) => {
  return (
    <div className="status">
      <p>{statusText}</p>
    </div>
  );
};

export default Status;
