import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import ShuffleButton from './ShuffleButton';

describe('ShuffleButton Component', () => {
  test('button renders with correct text', () => {
    render(<ShuffleButton onClick={() => {}} />);
    expect(screen.getByRole('button', {name: /shuffle/i})).toBeInTheDocument();
  });

  test('calls onClick when button is clicked', async () => {
    const handleClick = jest.fn();
    render(<ShuffleButton onClick={handleClick} />);
    
    await userEvent.click(screen.getByRole('button'));
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});



