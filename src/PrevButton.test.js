import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import PrevButton from './PrevButton';

describe('PrevButton Component', () => {
  test('calls onClick prop when clicked', async () => {
    const handleClick = jest.fn();
    render(<PrevButton onClick={handleClick} />);
    await userEvent.click(screen.getByRole('button', { name: /prev/i }));
    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
